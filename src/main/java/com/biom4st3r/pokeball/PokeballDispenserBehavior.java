package com.biom4st3r.pokeball;

import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.world.World;
import net.minecraft.block.Block;
import net.minecraft.block.dispenser.DispenserBehavior;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.entity.projectile.ArrowEntity;

public class PokeballDispenserBehavior implements DispenserBehavior {

    @Override
    public ItemStack dispense(BlockPointer bPointer, ItemStack pokeball) {
        if(pokeball.getTag().containsKey(PokeballItem.heldNpcName))
        {
            World w = bPointer.getBlockEntity().getWorld();
            w.spawnEntity(PokeballItem.makeEntityFromPokeball(pokeball, w, bPointer.getBlockPos()));
        }
        return null;
    }
    
}
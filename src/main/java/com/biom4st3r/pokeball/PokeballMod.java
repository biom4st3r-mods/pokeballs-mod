package com.biom4st3r.pokeball;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCategory;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.mob.DrownedEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.MobSpawnerLogic;
import net.minecraft.world.World;
import net.minecraft.world.loot.LootTables;
import net.minecraft.world.loot.entry.LootTableEntry;

public class PokeballMod implements ModInitializer {
	public static final String MODID = "biom4st3r";

	public static EntityType<ThrownPokeballEntity> Thrown_Pokeball;


	@Override
	public void onInitialize() 
	{
		/*
		Thrown_Pokeball = (EntityType)Registry.register(
			Registry.ENTITY_TYPE,
			new Identifier(MODID, "thrownpokeball"),
			FabricEntityTypeBuilder.create(EntityCategory.MISC,(t,w)->
			{
				return new ThrownPokeballEntity(t,w);
			}).size(0.25f,0.25f).build());
			*/
		Registry.register(Registry.ITEM,new Identifier(MODID,"pokeball"),new PokeballItem());
		//DispenserBlock.registerBehavior(PokeballItem::new, new PokeballDispenserBehavior());
	}
}

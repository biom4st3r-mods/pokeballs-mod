package com.biom4st3r.pokeball;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.system.CallbackI.P;

import net.minecraft.client.render.entity.PlayerEntityRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.TextComponent;
import net.minecraft.text.TranslatableTextComponent;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PokeballItem extends Item {
	public static final Logger logger = LogManager.getLogger();

	public static final String heldNpcTags = PokeballMod.MODID + ".NPCTag";
	public static final String heldNpcName = PokeballMod.MODID + ".NPCName";

	public PokeballItem() {
		super(new Settings().stackSize(16).itemGroup(ItemGroup.MISC));

	}

/*
	@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity pe, Hand hand) {

		ItemStack iS = pe.getStackInHand(hand);
		if (!pe.abilities.creativeMode) {
			iS.subtractAmount(1);
		}

		world.playSound((PlayerEntity)null, pe.x, pe.y, pe.z, SoundEvents.ENTITY_SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (random.nextFloat() * 0.4F + 0.8F));
		if (!world.isClient) {
			ThrownPokeballEntity tpe = new ThrownPokeballEntity(world, pe);
			tpe.setItem(iS);
			tpe.method_19207(pe, pe.pitch, pe.yaw, 0.0F, 1.5F, 1.0F);
			world.spawnEntity(tpe);
		}

		//return new TypedActionResult(ActionResult.SUCCESS, iS);
		return new TypedActionResult<ItemStack>(ActionResult.SUCCESS, iS);
	}
	*/
	
	private static String Capitalize(String s) {
		String t = s.substring(0, 1);
		return t.toUpperCase() + s.substring(1);
		
	}

	public static boolean isFilled(ItemStack iS)
	{
		try
		{
			return iS.getTag().containsKey(heldNpcName);
		}
		catch(NullPointerException e)
		{
			return iS.toTag(new CompoundTag()).containsKey(heldNpcName);
		}
		
	}
	public static ItemStack makePokeballFromEntity(Entity e, ItemStack iS) {
		if (!(e instanceof PlayerEntity) && !isFilled(iS)) {
			String mobName = EntityType.getId(e.getType()).toString(); // returned mob name string i.E. minecraft:cow
			if (iS.getTag() == null) {
				iS.setTag(new CompoundTag());
			}
			iS.getTag().put(heldNpcTags, e.toTag(new CompoundTag()));
			iS.getTag().putString(heldNpcName, mobName);
			e.world.addParticle(ParticleTypes.SMOKE, e.getPos().x, e.getPos().y+1, e.getPos().z, 0, 0, 0);
			e.remove();
		}
		
		return iS;
	}

	public static Entity makeEntityFromPokeball(ItemStack iS, World w, BlockPos blockpos) {
		if (iS.getItem() instanceof PokeballItem) {
			CompoundTag saferTag = (CompoundTag) iS.getTag().getTag(heldNpcTags);
			saferTag.remove("Pos"); // Pos was retained, because reflection
			saferTag.remove("UUIDLeast"); // matching UUID errors
			saferTag.remove("UUIDMost"); // matching UUID errors

			EntityType<?> pokemon = EntityType.get(iS.getTag().getString(heldNpcName)).get();

			Entity e = pokemon.create(w);
			try {
				Method badSolution2 = e.getClass().getMethod("method_5749", CompoundTag.class);// EntityWriteNbtData.getName(),CompoundTag.class);
				badSolution2.setAccessible(true);
				badSolution2.invoke(e, saferTag);
				e.setPosition(blockpos.up().getX(), blockpos.up().getY(), blockpos.up().getZ());
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InvocationTargetException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return e;
		}
		return null;
	}

	@Override
	public boolean interactWithEntity(ItemStack iS, PlayerEntity pE, LivingEntity le, Hand hand) {

		if (!isFilled(iS)) {
			ItemStack caughtMob = new ItemStack(iS.getItem(), 1);
			caughtMob = makePokeballFromEntity(le, caughtMob);


			if (pE.inventory.getEmptySlot() != -1) // Drop item if full inventory
			{
				pE.inventory.insertStack(caughtMob);
			} else {
				pE.dropStack(caughtMob);
			}
			iS.subtractAmount(1);
		}
		return true;
	}

	@Override
	@Environment(EnvType.CLIENT)
	public boolean hasEnchantmentGlint(ItemStack iS) {
		CompoundTag data = iS.getTag();
		try {
			return data.containsKey(heldNpcName) || data.containsKey(heldNpcTags);
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext iuc) {
		if (isFilled(iuc.getItemStack())) {
			BlockPos bp = iuc.getBlockPos();
			Entity e = makeEntityFromPokeball(iuc.getItemStack(), iuc.getWorld(), bp);
			iuc.getWorld().spawnEntity(e);
			iuc.getWorld().addParticle(ParticleTypes.FLAME,bp.getX(),bp.getY(),bp.getZ(),5,5,5);
			iuc.getWorld().playSoundFromEntity(iuc.getPlayer(), e, SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.BLOCKS,
					5f, 5f);
			iuc.getItemStack().addAmount(-1);
		}
		return super.useOnBlock(iuc);
	}

	@Override
	public TextComponent getTranslatedNameTrimmed(ItemStack iS) {
		try
		{
			if (isFilled(iS)) {
				return new TranslatableTextComponent(displayMobName(iS) + " in a ball");
			}
		}
		catch(NullPointerException e)
		{
			return new TranslatableTextComponent("Pokeball");
		}
		
		return new TranslatableTextComponent("Pokeball");
	}

	public String displayMobName(ItemStack iS) {
		String name = iS.getTag().getString(heldNpcName);
		return Capitalize(name.substring((name.indexOf(':') + 1))).replace('_', ' ');
	}




}


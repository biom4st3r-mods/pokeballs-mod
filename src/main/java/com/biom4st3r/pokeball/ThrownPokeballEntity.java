
package com.biom4st3r.pokeball;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.thrown.ThrownItemEntity;
import net.minecraft.item.EnderPearlItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;


public class ThrownPokeballEntity extends ThrownItemEntity {

    public ThrownPokeballEntity(EntityType<Entity> t, World w) {
        super(PokeballMod.Thrown_Pokeball,w);
    }

    //@Environment(EnvType.CLIENT)
    public ThrownPokeballEntity(World world_1, double double_1, double double_2, double double_3) {
       super(PokeballMod.Thrown_Pokeball, double_1, double_2, double_3, world_1);
    }


    public ThrownPokeballEntity(World world)
    {
        super(PokeballMod.Thrown_Pokeball,world);
    }


	public ThrownPokeballEntity(World world, LivingEntity pe) {
        super(PokeballMod.Thrown_Pokeball, pe.getBlockPos().getX(), pe.getBlockPos().getY(), pe.getBlockPos().getZ(),
                world);
	}

	@Override
    protected Item getDefaultItem() {
        EnderPearlItem
        return new PokeballItem();
    }
    
    @Override
    protected void onCollision(HitResult hit) {
        CompoundTag item = getItem().getTag();

        boolean containsMob = item.containsKey(PokeballItem.heldNpcName) || item.containsKey(PokeballItem.heldNpcTags);
        if(containsMob) // filled pokeball
        {
            PokeballItem.makeEntityFromPokeball(this.getItem(),this.world, new BlockPos(hit.getPos()));
        }
        else if (hit.getType() == HitResult.Type.ENTITY) // hits entity
        {
            Entity le = ((EntityHitResult) hit).getEntity();
            BlockPos b = le.getBlockPos();
            this.world.spawnEntity(new ItemEntity(world, b.getX(), b.up().getY(), b.getZ(), PokeballItem.makePokeballFromEntity(le,this.getItem())));
        }
        else if(hit.getType() == HitResult.Type.BLOCK) // hits block
        {
            BlockPos b = ((BlockHitResult)hit).getBlockPos();
            this.world.spawnEntity(new ItemEntity(this.world, b.getX(), b.up().getY(), b.getZ(), new ItemStack(getDefaultItem(), 1)));
        }
        MinecraftClient.getInstance().player.sendChatMessage("collision");
        this.remove();
    }

    @Override
    protected void initDataTracker() {

    }
}

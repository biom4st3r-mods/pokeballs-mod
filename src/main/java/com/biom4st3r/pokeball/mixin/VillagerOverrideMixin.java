package com.biom4st3r.pokeball.mixin;

import com.biom4st3r.pokeball.PokeballItem;
import net.minecraft.entity.passive.VillagerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Hand;
import org.spongepowered.asm.mixin.Debug;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;


@Mixin(VillagerEntity.class)
public class VillagerOverrideMixin
{

    //@Inject(at = @At("INVOKE"), method = "interactMob")
    @Debug()
    @Inject(at = @At("HEAD"),method = "interactMob",cancellable = true)
    public void interactMob(PlayerEntity pE, Hand hand,CallbackInfoReturnable<Boolean> ci)
    {
        if(pE.getStackInHand(hand).getItem() instanceof PokeballItem)
        {
            //ci.setReturnValue(true);
            ci.cancel();
        }

    }

}
